# Dockerfile to use with heroku
# Build using:
#  docker build --rm -t instant-salon-api .
#
#
FROM ubuntu:16.04

MAINTAINER Herald Olivares<heraldmatias.oz@gmail.com>

RUN apt-get clean; apt-get update; apt-get -y upgrade
RUN apt-get -y install libssl-dev openssl; apt-get autoclean
RUN apt-get -y install gcc curl; apt-get autoclean
RUN apt-get -y install python3-pip; apt-get autoclean
RUN apt-get -y install python3.5-dev python3.5-venv python3-setuptools; apt-get autoclean
RUN pip3 install --upgrade pip; pip install virtualenv
RUN apt-get autoremove

RUN mkdir -p /opt/instantsalon_api/
COPY . /opt/instantsalon_api/.
WORKDIR /opt/instantsalon_api
RUN virtualenv venv

RUN /bin/bash -c "source /opt/instantsalon_api/venv/bin/activate && pip install -r requirements.txt && deactivate"

# THIS ONLY FOR LOCAL TESTING
# EXPOSE 8000

CMD /opt/instantsalon_api/venv/bin/gunicorn --bind 0.0.0.0:$PORT instantsalon_api.wsgi:application