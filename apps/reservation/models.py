from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings

class User(AbstractUser):
    """
    Representa un cliente de instant salon
    """
    birth_date = models.DateField(null=True)


class Salon(models.Model):
    """
    Salon que provee servicios
    """
    name = models.CharField(max_length=150, db_index=True) # indexamos para hacer mas eficiente la busqueda por nombre de salon
    address = models.CharField(max_length=250)
    services = models.ManyToManyField(to="Service") # los 10 primeros servicios de la relacion

    def __str__(self):
        return self.name

class Service(models.Model):
    """
    Servicios que se proveen en los salones
    """
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2, db_index=True)
    duration = models.IntegerField(db_index=True) # almacena la duracion en minutos

    def __str__(self):
        return self.name

class Reservation(models.Model):
    """
    Representa una reserva agendada hecha por un usuario
    """
    salon = models.ForeignKey(to=Salon)
    service = models.ForeignKey(to=Service)
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL)
    created_date = models.DateTimeField(auto_now_add=True)
    start_reservation_date = models.DateField()
    end_reservation_date = models.DateField()
    start_reservation_time = models.TimeField()  # desde que hora puedo reservar este servicio?
    end_reservation_time = models.TimeField()


class ReservationLock(models.Model):
    """
    Representa un intento de reservacion de un servicio, tiene un tiempo de expiracion
    el tiempo de expiracion es controlado por la setting: RESERVATION_LOCK_TIMEOUT
    """
    salon = models.ForeignKey(to=Salon)
    service = models.ForeignKey(to=Service)
    user = models.ForeignKey(to=User)
    created_date = models.DateField(auto_now_add=True)
    start_attempt_date = models.DateTimeField(auto_now_add=True)
    end_attempt_date = models.DateTimeField()
    is_valid = models.BooleanField(default=True)


class ScheduleServiceSalon(models.Model):
    """
    Representa el horario en que se puede atender determinado servicio en el salon.
    Se puede programar como recurrente que significa que se tiene un mismo horario para determinado
    servicio en un salon.
    Esta entidad se utilizara para controlar las reservaciones

    """
    salon = models.ForeignKey(to=Salon)
    service = models.ForeignKey(to=Service)
    created_date = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField() # fecha en que empieza a ser valido el horario
    end_date = models.DateField(null=True) # fecha en que termina la validez del horario
    start_time = models.TimeField() # desde que hora puedo reservar este servicio?
    end_time = models.TimeField() # hasta que hora puedo reservar este servicio?
    capacity = models.IntegerField(db_index=True)  # ¿cuantas personas puedo atender para ese servicio en dicho horario
    # en un mismo salon?
    is_recurring = models.BooleanField(default=False) # este horario es valido todos los dias?


class ScheduleServiceSalonUsage(models.Model):
    # almacena el uso en "capacidad" de atencion de un horario de atencion en una determinada fecha
    # esto sirve para controlar las reservas
    schedule = models.ForeignKey(to=ScheduleServiceSalon) # horario que se esta utilizando
    current_date = models.DateField() # fecha en que se estan haciendo las reservas
    usage = models.IntegerField() # personas que han reservado el servicio, si esto es igual a la capacidad
    # entonces ya no se podra hacer mas reservas del servicio en este dia