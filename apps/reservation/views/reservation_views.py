from datetime import datetime, timedelta

from django.conf import settings
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import list_route, detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.reservation.models import Service, Reservation, ReservationLock
from apps.reservation.pagination import GenericNumberPagination
from apps.reservation.serializers import ServiceListSerializer, ReservationSerializer, ReservationCreateSerializer, \
    ReservationLockSerializer


class ReservationManagementViewSet(viewsets.ModelViewSet):
    pagination_class = GenericNumberPagination
    queryset = Reservation.objects.all()
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        return ReservationSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        data = request.data
        # flujo de reservacion, no estoy controlando cuantas reservas de un mismo servicio puede hacer el cliente
        # al día en un salon, asumo que puede ir las veces que quiera el mismo dia, quiza exista algun criterio para ello.
        # lo que si estoy controlando es que las reservas se hagan en diferentes horas de tal manera que no se sobrelapen
        serializer = ReservationCreateSerializer(data=data,default={'user': user})
        serializer.is_valid(raise_exception=True) # si la validacion es correcta el flujo continua
        # HTTP 400 en caso falle el flujo de validacion

        obj = serializer.save()
        return Response(data=self.get_serializer(obj).data)

    @list_route(methods=['post'], url_path=r'start')
    def start_reservation(self, request, *args, **kwargs):
        # me imagino a la reserva como un flujo de por lo menos 3 pasos
        # paso previo al inicio del flujo de reservacion consulta datos del salon y servicios
        # 1. asumo que una vez que escoge el servicio de un salon X, el sistema lo lleva al primer paso de
        # la reservacion, internamente se inicia un contador de 5 minutos para que en ese lapso se confirme o no la
        # reserva
        # 2. asumo que escoge algun tipo de informacion mas como medio de pago etc (Esto no estoy implementando)
        # 3. se confirma la reservacion y se guardan los datos en las tablas correspondientes (esto esta imeplementado,
        # en el metodo create)
        # No estoy implementado un endpoint para el cancelar la reserva antes de ser contemplada
        # de momento solo he implementado el posible paso 1 y el ultimo paso.
        user = request.user

        defaults = {
            'created_date': datetime.now().date(),
            'start_attempt_date': datetime.now(),
            'end_attempt_date': datetime.now()  + timedelta(seconds=settings.RESERVATION_LOCK_TIMEOUT)
        }

        serializer = ReservationLockSerializer(data=request.data)
        serializer.is_valid(raise_exception=True) # si la validacion es correcta el flujo continua
        # HTTP 400 en caso falle el flujo de validacion

        reservation_attempt, created = ReservationLock.objects.get_or_create(
            service_id=request.data['service'],
            salon_id=request.data['salon'],
            user_id=user.id,
            is_valid=True,
            defaults=defaults
        )

        return Response({'attempt': reservation_attempt.id})

    @list_route(methods=['get'], url_path=r'services')
    def search_services(self, request, *args, **kwargs):
        # endpoint para buscar servicios
        # se puede buscar por nombre y/o ordenar los resultados
        filter_name = self.request.GET.get('name', None)
        ordering = self.request.GET.get('order', None)

        queryset = Service.objects.all()
        if filter_name:
            queryset = queryset.filter(name__icontains=filter_name)

        if ordering:
            queryset = queryset.order_by(ordering)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = ServiceListSerializer(queryset, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = ServiceListSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['get'], url_path=r'services/(?P<pk>[^/.]+)')
    def detail_service(self, request, *args, **kwargs):
        # endpoint para buscar servicios
        # se puede buscar por nombre y/o ordenar los resultados

        queryset = Service.objects.all()
        obj = get_object_or_404(queryset, **kwargs)

        serializer = ServiceListSerializer(obj)
        return Response(serializer.data)
