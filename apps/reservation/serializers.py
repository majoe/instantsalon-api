from datetime import datetime, timedelta

from django.db import transaction
from django.db.models.expressions import F
from rest_framework import serializers

from apps.reservation.models import Service, Reservation, ScheduleServiceSalon, ReservationLock, \
    ScheduleServiceSalonUsage


class AuthSerializer(serializers.Serializer):
    full_name = serializers.CharField(max_length=150)
    token = serializers.CharField(max_length=30)

    def get_full_name(self, instance):
        return instance.get_full_name()


class ServiceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = '__all__'


class ReservationLockSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReservationLock
        fields = ('salon', 'service')


class ReservationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = ('salon', 'service', 'start_reservation_date', 'start_reservation_time')

    def validate(self, attrs):
        user = self.default['user']
        # 1 verificar si hay un horario valido para la reserva, segun la fecha y hora en la que quiere reservar
        # el cliente
        schedule_qs = ScheduleServiceSalon.objects.filter(start_date__lte=attrs['start_reservation_date'],
                                                          end_date__gte=attrs['start_reservation_date'],
                                                          start_time__lte=attrs['start_reservation_time'],
                                                          end_time__gte=attrs['start_reservation_time'],
                                                          service=attrs['service'],
                                                          salon=attrs['salon'])
        if not schedule_qs.exists():
            # sera recurrente?
            schedule_qs = ScheduleServiceSalon.objects.filter(
                start_time__lte=attrs['start_reservation_time'],
                end_time__lte=attrs['start_reservation_time'],
                service=attrs['service'],
                salon=attrs['salon'], is_recurring=True)

        if not schedule_qs.exists():
            raise serializers.ValidationError("No se puede reservar en el horario solicitado", code='support')

        # encontrammos un horario de atencion valido
        schedule = schedule_qs.get()

        # 2 ¿Tiene ya una reserva dentro del horario que solicita?
        start_dt = datetime.combine(attrs['start_reservation_date'],
                                    attrs['start_reservation_time'])

        reservation_qs = Reservation.objects.filter(
            service=attrs['service'],
            salon=attrs['salon'],
            user=user,
            start_reservation_date__exact = attrs['start_reservation_date'],
            start_reservation_time__lte=(start_dt + timedelta(minutes=attrs['service'].duration)).time(),
            end_reservation_time__gte=attrs['start_reservation_time']
        )

        if reservation_qs.exists():
            ReservationLock.objects.filter(
                service=attrs['service'],
                salon=attrs['salon'],
                user_id=user.id,
                created_date=datetime.now().date(),
                is_valid=True
            ).update(is_valid=False)

            raise serializers.ValidationError(
                "Usted ya tiene una reserva en un horario similar, programe su reserva en otra hora", code='support')

        # 2.1 verificar si hay un "intento" de reserva
        reservation_attempt_qs = ReservationLock.objects.filter(
            service=attrs['service'],
            salon=attrs['salon'],
            user_id=user.id,
            created_date=datetime.now().date(),
            start_attempt_date__lte=datetime.now(),
            end_attempt_date__gte=datetime.now(),
            is_valid=True
        )

        if not reservation_attempt_qs.exists():
            # las que expiraron las desactivamos, esto deberia hacerlo un cron o algun componente en background
            # para fines de prueba del flujo de reserva he agregado estas lineas
            ReservationLock.objects.filter(
                service=attrs['service'],
                salon=attrs['salon'],
                user_id=user.id,
                created_date=datetime.now().date(),
                is_valid=True
            ).update(is_valid=False)

            raise serializers.ValidationError("La reserva no ha sido iniciada", code='support')

        reservation_attempt = reservation_attempt_qs.first()

        # 3 verificar si aun hay capacidad para atender nuestra reserva en la fecha solicitada por el cliente

        usage_obj, created = ScheduleServiceSalonUsage.objects.get_or_create(
            schedule=schedule,
            current_date=attrs['start_reservation_date'],
            defaults={'usage': 0}
        )

        capacity = schedule.capacity
        usage = ReservationLock.objects.filter(
            service=attrs['service'],
            salon=attrs['salon'],
            created_date=datetime.now().date(),
            is_valid=True
        ).count() + usage_obj.usage

        if not capacity > usage:
            raise serializers.ValidationError("Se han agotado las reservas en el horario solicitado", code='support')

        # 4. fecha y hora de la reserva que sea mayor a la actual
        if attrs['start_reservation_date'] < datetime.now().date():
            raise serializers.ValidationError("Fecha de reserva debe ser mayor a la fecha actual", code='support')

        if attrs['start_reservation_date'] == datetime.now().date() and attrs['start_reservation_time'] < datetime.now().time():
            raise serializers.ValidationError("Hora de reserva debe ser mayor a la hora actual", code='support')

        attrs['reservation_attempt'] = reservation_attempt
        attrs['usage'] = usage_obj

        return attrs

    def save(self, **kwargs):
        reservation_attempt = self.validated_data['reservation_attempt']
        usage = self.validated_data['usage']
        user = self.default['user']

        with transaction.atomic():
            # 1. desactivamos el intento de reserva actual para que se pueda efectuar otra reserva
            reservation_attempt.is_valid = False
            reservation_attempt.save()

            # 2. actualizamos el uso de la capacidad en 1
            usage.usage = F('usage') + 1
            usage.save()
            start_dt= datetime.combine(self.validated_data['start_reservation_date'], self.validated_data['start_reservation_time'])

            # registramos la reserva
            instance = Reservation(
                user=user,
                salon=self.validated_data['salon'],
                service=self.validated_data['service'],
                start_reservation_date=self.validated_data['start_reservation_date'],
                start_reservation_time=self.validated_data['start_reservation_time'],
                end_reservation_date=self.validated_data['start_reservation_date'] + timedelta(minutes=self.validated_data['service'].duration),
                end_reservation_time=(start_dt + timedelta(minutes=self.validated_data['service'].duration)).time()
            )
            instance.save()

        return instance
