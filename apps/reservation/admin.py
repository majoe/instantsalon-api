from django.contrib import admin
from apps.reservation.models import Salon, Service, ScheduleServiceSalon

class SalonAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'duration', 'price')

class ScheduleServiceSalonAdmin(admin.ModelAdmin):
    list_display = ('salon', 'service', 'start_date', 'end_date', 'start_time', 'end_time')


admin.site.register(Salon, SalonAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(ScheduleServiceSalon, ScheduleServiceSalonAdmin)
