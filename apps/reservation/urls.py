from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from apps.reservation.views import auth_views, reservation_views

urlpatterns = [
    url(r'^api-token-auth/', auth_views.CustomAuthToken.as_view())
]
router = DefaultRouter()
# auto configuracion de URLS
router.register(r'reservation', reservation_views.ReservationManagementViewSet, 'reservation')

urlpatterns += router.urls
