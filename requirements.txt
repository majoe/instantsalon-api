Django==1.11.3
django-debug-toolbar==1.8
psycopg2==2.7.1
gunicorn==19.9.0
django-cors-headers==2.1.0
djangorestframework==3.6.3
